#!/usr/bin/python3

from flask import Flask
import git
from git import Repo
import requests, json, docker, os

# Function for cloning of the local depot
t="graphviz_workss"
Build_DIR="/home/ubuntu/TP_704/flask_pr/git_graph"
clone_DIR_="/home/ubuntu/TP_704/flask_pr/git_graph"
client = docker.from_env()


def clone(dep, dire):
        #git.Repo.clone_from(dep, dire, branch='master')
        try:
           repo = Repo(dire)
           for url in repo.remotes.origin.urls:
                  
                if url==dep:
                       print("Dépot "+(dep.split('/')[-1]).replace('.git','')+" déjà cloné dans "+repo.working_dir)
        except Exception as e:
            repo = Repo.clone_from(dep, dire)
            print("Dépot "+(dep.split('/')[-1]).replace('.git','')+" cloné dans "+repo.working_dir)
        return repo     
    

#Function to build the docker image from the dockerfile to solve the task.  
def build_container():
        fname=Build_DIR
        client.images.build(path=fname,tag=t)
#_________________________________________________________________________________________
#Function to start the container and return the result of the task 
def run_container(image):
        res = client.containers.run(image)
        return res
###-----------------Update depot_git------------------########################################

def Add_To_Depot():

    repo = git.Repo('/home/ubuntu/TP_704/flask_pr/git_graph')
    repo.git.add('/home/ubuntu/TP_704/flask_pr/git_graph/p.png')
    repo.git.commit('-m', 'test commit', author='adamadiaa1994@gmail.com')
    repo.remotes.origin.push(refspec='master:master')

r = requests.get("http://localhost:5000/rabbit/TODO")   
while r.status_code == 200:

        print("statut : {}".format(r.status_code))

# return the response of the query to the variable and convert it  
        tache = json.loads(r.text)

# we clone the repository locally from the address of the git that contains a dockerfile and the script of the task
        print("--Clonage du depot------")
        clone(dep=tache["address_git"], dire=clone_DIR_)
 #we build the alpine image that will execute the task 
        print("build the docker for the task")
        build_container()
# we start the created container and return the result in the variable d
        print("Run the tache docker and return a result")
        #d = run_container(image=t)
        os.system("docker  run -d -v '/home/ubuntu/TP_704/flask_pr/git_graph:/graphviz' graphviz_workss:latest")
            
# we build the code of the result task which contains the project id and task id previously returned
        ta_res = {}
        ta_res["id_projet"] = tache["id_projet"]
        ta_res["id_tache"] = tache["id_tache"]
        #os.system('docker exec doc_works  ')
        #on serialise le resultat pour qu on peut la renvoyer
        ta_res["result_tache"]= "Task_Done"
        #ta_res["resultat"] = json.dumps(d.decode("utf-8"))
        tach_res = {"ta" : json.dumps(ta_res)}
        print(tach_res)

# Then we send the result to the DONE queue.
        print("send task to queue DONE")
        r1 = requests.post("http://localhost:5000/rabbit/DONE", data=tach_res)
        print("statut : {}".format(r.status_code))
        Add_To_Depot()
else:
        print("la queue est vide")