#!/usr/bin/python3

from flask import Flask
import requests
import json
import pika


# CREATION DES FILES "TODO" ET "DONE"
list_queue = {"TODO", "DONE"}
for i in list_queue:
        que = {}
        que["file"] = i
        d = {"queue" : json.dumps(que)}
        r = requests.post("http://localhost:5000/rabbit", data=d)
        print("statut : {}".format(r.status_code))
        print(r.text)
#Generation de la tache a resoudre
tache = {}
tache["id_projet"] = 100
tache["id_tache"] = 10
tache["address_git"] = "https://gitlab.com/dams94/project.git"
tache["command"] = "python3 tache.py"
#Conversion du python en JSON
task = {"ta" : json.dumps(tache)}

#Envoi de la tache au file TO DO
r = requests.post("http://localhost:5000/rabbit/TODO", data=task)
print("statut : {}".format(r.status_code))
print(r.text)

connection = pika.BlockingConnection(pika.ConnectionParameters(host='10.0.2.15'))
channel = connection.channel()
def callback(ch, method, properties, body):
    print("reception de %r" % body)

channel.basic_consume(queue="DONE", on_message_callback=callback, auto_ack=True)
print("en attent s'un message")
rss = channel.start_consuming()

print (rss)